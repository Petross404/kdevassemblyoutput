#include "kdevassemblyoutput.h"

#include <KCoreAddons/KAboutData>
#include <KCoreAddons/KPluginFactory>
//#include <KDELibs4Support/KDE/KLocale>

#include <QtCore/QVariantList>


K_PLUGIN_FACTORY(KDevassemblyoutputFactory,
		registerPlugin<kdevassemblyoutput>();)

K_EXPORT_PLUGIN(KDevassemblyoutputFactory(
	KAboutData("kdevassemblyoutput", 
			"kdevassemblyoutput",
			ki18n("assemblyoutput"),
			"0.1", 
			ki18n("An example plugin for KDevelop"),
			KAboutData::License_GPL)
))


kdevassemblyoutput::kdevassemblyoutput( QWidget* parent, const QVariantList& args )
: KDevelop::IPlugin( componentName(), 
				parent)
	, m_ui( new kdevassemblyoutputconfig( parent ) ) 
{
	Q_UNUSED(args);
}

void kdevassemblyoutput::createActions( Sublime::MainWindow* window, QString xmlFile, KActionCollection& actions )
{
	xmlFile = QStringLiteral("kdevassemblyoutput.rc");
	QAction* openConfigAction = actions.addAction(QStringLiteral("config"));
	openConfigAction->setText(QStringLiteral("Open Configuration window"));

	connect(openConfigAction, &QAction::triggered, m_ui , &kdevassemblyoutputconfig::show);
}

kdevassemblyoutput::~kdevassemblyoutput()
{
	
}

#include "kdevassemblyoutput.moc"
