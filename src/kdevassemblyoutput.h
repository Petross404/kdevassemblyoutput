#ifndef KDEVASSEMBLYOUTPUT_H
#define KDEVASSEMBLYOUTPUT_H


#include <interfaces/iplugin.h>
#include <QtCore/QVariantList>

#include "kdevassemblyoutputconfig.h"

class kdevassemblyoutput : public KDevelop::IPlugin
{
    Q_OBJECT
    
public:
	kdevassemblyoutput( QWidget* parent, const QVariantList& args = QVariantList();
	void	createActions(Sublime::MainWindow* window, QString xmlFile,  KActionCollection& actions);

	virtual ~kdevassemblyoutput();

private:
	kdevassemblyoutputconfig	m_ui;
};

#endif     // KDEVASSEMBLYOUTPUT_H
